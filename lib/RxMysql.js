const Rxjs = require("rx");
const Observable = Rxjs.Observable;
const Sql = require("mysql");


let _pool = null;

function RxMysql($configs) {
    let _mysqlConfigs = $configs;

    Object.defineProperties(this, {
        configs: {
            get: () => _mysqlConfigs,
            set: ($configs) => {
                _pool = Sql.createPool($configs);
                _mysqlConfigs = $configs;
            },
            enumerable: true
        }
    })
}

RxMysql.getConnection = function($configs) {
    return Observable.create(
        $observer => {
            try {
                if(!_pool && !$configs) {
                    $observer.onError({
                        errors: true,
                        messages: [
                            "errore nel recupero della connessione",
                            "non ci sono pool disponibili e non sono stati passati parametri di configurazione"
                        ],
                        errData: {}
                    });
                }
                let equal = Object.keys(RxMysql).map( e => {
                    return RxMysql[e] == $configs[e];
                }).reduce( (equal, e) => {
                    return equal && e;
                }, true);
                if(!equal && _pool) {
                    _pool = null;
                }
                if(!_pool) {
                    _pool = Sql.createPool($configs);
                }

                _pool.getConnection(
                    ($err, connection) => {
                        if($err) {
                            $observer.onError($err);
                        } else {
                            $observer.onNext(connection);
                            $observer.onCompleted();
                        }
                    }
                );
            } catch ($err) {
                return $observer.onError($err);
            }

        }
    );
}

RxMysql.end = function($callback) {
    if(typeof $callback == 'undefined') {
        _pool.end();
    } else {
        _pool.end($callback);
    }
}

RxMysql.prototype = Object.create(Object.prototype, {
    constructor: {
        value: RxMysql,
        enumerable: true
    }
});

RxMysql.prototype.connect = function() {
    return RxMysql.getConnection(this.configs).map( $connection => {
        this.connection = $connection;
        return $connection;
    });
}

RxMysql.prototype.end = function() {
    this.release();
}

RxMysql.prototype.release = function() {
    if(this.connection) {
        this.connection.release();
        this.connection = null;
    }
}

RxMysql.prototype.destroy = function() {
    if(this.connection) {
        this.connection.destroy();
        this.connection = null;
    }
}

RxMysql.prototype.select = function($query) {
    let _this = this;
    return this.connect().flatMapObserver(
        $_connection => {
            return Observable.create( $observer => {
                $_connection.query($query, ($_err, $_res, $_fields) => {
                    if($_err) {
                        $observer.onError($_err);
                    } else {
                        $observer.onNext({
                            rows: $_res,
                            fields: $_fields
                        });
                        $observer.onCompleted();
                    }
                    _this.end();
                });
            });
        },
        $_err => {
            return Observable.throw($_err);
        },
        () => {
            return Observable.empty();
        }
    );
}

RxMysql.prototype.selectStreaming = function($query) {
    let _this = this;
    return this.connect().flatMapObserver(
        $_connection => {
            return Observable.create( $observer => {
                $_connection.query($query)
                    .on("error", ($_err) => {
                        $observer.onError($_err);
                        _this.end();
                    })
                    .on("fields", $_fields => {
                        $observer.onNext($_fields);
                    })
                    .on("result", $_row => {
                        $observer.onNext($_row);
                    })
                    .on("end", () => {
                        $observer.onCompleted();
                        _this.end();
                    });
            });
        },
        $_err => {
            return Observable.throw($_err);
        },
        () => {
            return Observable.empty();
        }
    )
}
module.exports = RxMysql;
